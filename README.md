# 4 beginner

## Howto

Container starten/builden

```bash
docker compose up -d
```

Spiel spielen:

```bash
docker exec -it 2_expert-number-guessing-1 ./NumberGuessingdocker
```

Spiel nochmals spielen:

```bash
docker exec -it 2_expert-number-guessing-1 ./NumberGuessingdocker
```

Container stoppen:

```bash
docker compose stop
```

Container löschen:

```bash
docker compose rm
```
